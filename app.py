from flask import Flask, render_template, request, redirect, url_for, session, current_app, flash
from flask_mysqldb import MySQL
import bcrypt


app = Flask(__name__)


# MySQL Configuration
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_DB'] = 'electricity_bill_mgmt'

mysql = MySQL()
mysql.init_app(app)

secret_key = ''
# Routes for basic part (before authentication)


@app.route('/')
def landing_page():
    return render_template('index.html')


@app.route('/admin_login', methods=['GET', 'POST'])
def admin_login():
    if request.method == 'POST':
        # Fetch form data
        identifier = request.form.get('identifier')
        password = request.form.get('password')

        if not identifier or not password:
            error = 'Please provide both username/email and password'
            return render_template('admin_login.html', error=error)

        identifier = identifier.lower()

        cur = mysql.connection.cursor()
        cur.execute(
            "SELECT * FROM admins WHERE LOWER(Username) = %s OR LOWER(Email) = %s", (identifier, identifier))
        admin = cur.fetchone()
        cur.close()

        if admin:
            if bcrypt.checkpw(password.encode(), admin[2].encode()):
                return redirect(url_for('admin_dashboard'))
            else:
                error = 'Invalid password'
                return render_template('admin_login.html', error=error)
        else:
            error = 'Admin not found'
            return render_template('admin_login.html', error=error)
    return render_template('admin_login.html')


@app.route('/client_login', methods=['GET', 'POST'])
def client_login():
    if request.method == 'POST':
        # Fetch form data
        identifier = request.form.get('identifier')
        password = request.form.get('password')

        if not identifier or not password:
            error = 'Please provide both username/email and password'
            return render_template('client_login.html', error=error)

        identifier = identifier.lower()

        cur = mysql.connection.cursor()
        cur.execute(
            "SELECT * FROM users WHERE LOWER(Username) = %s OR LOWER(Email) = %s", (identifier, identifier))
        user = cur.fetchone()
        cur.close()

        if user:
            if bcrypt.checkpw(password.encode(), user[2].encode()):
                # Extract UserID from user tuple
                user_id = user[0]
                # Redirect to client_dashboard with user_id as parameter
                return redirect(url_for('client_dashboard', user_id=user_id))
            else:
                error = 'Invalid password'
                return render_template('client_login.html', error=error)
        else:
            error = 'User not found'
            return render_template('client_login.html', error=error)
    return render_template('client_login.html')


@app.route('/register_admin', methods=['GET', 'POST'])
def register_admin():
    if request.method == 'POST':
        username = request.form['username']
        email = request.form['email']
        password = request.form['password']
        fullname = request.form['fullname']

        hashed_password = bcrypt.hashpw(password.encode(), bcrypt.gensalt())

        cur = mysql.connection.cursor()
        cur.execute("INSERT INTO admins (Username, Email, Password, FullName) VALUES (%s, %s, %s, %s)",
                    (username, email, hashed_password, fullname))
        mysql.connection.commit()
        cur.close()

        return redirect(url_for('admin_login'))
    return render_template('register_admin.html')

# Routes after authentication


'''
Routes for user management 
'''


@app.route('/user_management')
def user_management():
    # Fetch data from the users table
    cur = mysql.connection.cursor()
    cur.execute(
        "SELECT UserID, Username, Email, FullName, Address, ContactNumber FROM users")
    users_data = cur.fetchall()
    cur.close()

    # Convert tuple data to a list of dictionaries
    users = [{'UserID': user[0], 'Username': user[1], 'Email': user[2], 'FullName': user[3],
              'Address': user[4], 'ContactNumber': user[5]} for user in users_data]

    return render_template('user_management.html', users=users)

# Route to add a new user


@app.route('/add_user', methods=['GET', 'POST'])
def add_user():
    if request.method == 'POST':
        # Fetch form data
        username = request.form['username']
        email = request.form['email']
        password = request.form['password']
        fullname = request.form['fullname']
        address = request.form.get('address')
        contact_number = request.form.get('contact_number')

        # Hash the password before storing
        hashed_password = bcrypt.hashpw(password.encode(), bcrypt.gensalt())

        # Establish database connection
        cur = mysql.connection.cursor()
        # Execute SQL query to insert a new user
        cur.execute("INSERT INTO users (Username, Password, Email, FullName, Address, ContactNumber) VALUES (%s, %s, %s, %s, %s, %s)",
                    (username, hashed_password, email, fullname, address, contact_number))
        # Commit the transaction
        mysql.connection.commit()
        # Close cursor
        cur.close()

        # Redirect to the view users page after successful addition
        return redirect(url_for('user_management'))

    # Render the add user form
    return render_template('add_user.html')

# Route to edit an existing user


@app.route('/edit_user/<int:user_id>', methods=['GET', 'POST'])
def edit_user(user_id):
    if request.method == 'POST':
        # Fetch form data
        username = request.form['username']
        email = request.form['email']
        fullname = request.form['fullname']
        address = request.form.get('address')
        contact_number = request.form.get('contact_number')

        # Establish database connection
        cur = mysql.connection.cursor()
        # Execute SQL query to update user details
        cur.execute("UPDATE users SET Username = %s, Email = %s, FullName = %s, Address = %s, ContactNumber = %s WHERE UserID = %s",
                    (username, email, fullname, address, contact_number, user_id))
        # Commit the transaction
        mysql.connection.commit()
        # Close cursor
        cur.close()

        # Redirect to the view users page after successful update
        return redirect(url_for('user_management'))

    # Establish database connection
    cur = mysql.connection.cursor()
    # Execute SQL query to fetch user details by ID
    cur.execute("SELECT * FROM users WHERE UserID = %s", (user_id,))
    # Fetch the user data as a tuple
    user_tuple = cur.fetchone()
    # Close cursor
    cur.close()

    # Convert the tuple into a dictionary
    user = {
        'UserID': user_tuple[0],
        'Username': user_tuple[1],
        'Password': user_tuple[2],
        'Email': user_tuple[3],
        'FullName': user_tuple[4],
        'Address': user_tuple[5],
        'ContactNumber': user_tuple[6]
    }

    # Render the edit user form with user data
    return render_template('edit_user.html', user=user)

# Route to delete a user


@app.route('/delete_user/<int:user_id>', methods=['POST'])
def delete_user(user_id):
    try:
        # Establish database connection
        cur = mysql.connection.cursor()
        # Execute SQL query to delete user by ID
        cur.execute("DELETE FROM users WHERE UserID = %s", (user_id,))
        # Commit the transaction
        mysql.connection.commit()
        # Close cursor
        cur.close()
        # Redirect to the view users page after successful deletion
        return redirect(url_for('user_management'))
    except Exception as e:
        # Display error message
        return render_template('error.html', message='Cannot delete active user.')


'''
end of user management route

'''


'''
routes for billing management 
'''
# Route to render the add_bill.html page


@app.route('/billing_management', methods=['GET'])
def billing_management():
    # Fetch list of bills
    cur = mysql.connection.cursor()
    cur.execute(
        "SELECT BillID, UserID, Year, Month, BillAmount, PaymentStatus FROM bills")
    bills = cur.fetchall()
    print(bills)
    cur.close()

    return render_template('billing_management.html', bills=bills)


# Route to handle form submission and add bill to the database

@app.route('/add_bill', methods=['GET', 'POST'])
def add_bill():
    # Retrieve list of users for dropdown
    cur = mysql.connection.cursor()
    cur.execute("SELECT UserID, Username FROM users")
    users = cur.fetchall()
    print(users)
    user_choices = [{'UserID': user[0], 'Username': user[1]} for user in users]
    cur.close()

    if request.method == 'POST':
        user_id = request.form['user_id']
        year = request.form['year']
        month = request.form['month']
        units_consumed = request.form['units_consumed']

        # Calculate bill amount
        rate_per_unit = 2.5
        bill_amount = float(units_consumed) * rate_per_unit

        # Insert bill details into the database
        cur = mysql.connection.cursor()
        cur.execute("INSERT INTO bills (UserID, Year, Month, BillAmount, PaymentStatus) VALUES (%s, %s, %s, %s, 'Unpaid')",
                    (user_id, year, month, bill_amount))
        mysql.connection.commit()
        cur.close()

        return redirect(url_for('billing_management'))
    else:
        return render_template('add_bills.html', user_choices=user_choices)


@app.route('/update_payment/<int:bill_id>/<string:payment_status>/<string:requester>', methods=['GET'])
def update_payment(bill_id, payment_status, requester):
    user_id = request.args.get('user_id')
    cur = mysql.connection.cursor()
    if payment_status == 'Paid':
        new_payment_status = 'Unpaid'
    else:
        new_payment_status = 'Paid'
    cur.execute("UPDATE bills SET PaymentStatus = %s WHERE BillID = %s",
                (new_payment_status, bill_id))
    mysql.connection.commit()
    cur.close()
    if requester == 'a':
        return redirect(url_for('billing_management'))
    else:
        if user_id:
            return redirect(url_for('client_dashboard', user_id=user_id))
        else:
            return render_template('client_dashboard.html')


'''
routes for billing management  ends
'''


'''
detailed statistics route 
'''


@app.route('/detailed_statistics')
def detailed_statistics():
    try:
        # Get total number of users
        cur = mysql.connection.cursor()
        cur.execute("SELECT COUNT(*) FROM users")
        total_users = cur.fetchone()[0]

        # Get total unit consumed segregated by month
        cur.execute(
            "SELECT Year, Month, SUM(BillAmount) AS TotalBillAmount FROM bills GROUP BY Year, Month")
        unit_consumption = cur.fetchall()

        # Get total revenue generated
        cur.execute(
            "SELECT SUM(BillAmount) FROM bills WHERE PaymentStatus = 'Paid'")
        total_paid_revenue = cur.fetchone()[0]
        cur.execute(
            "SELECT SUM(BillAmount) FROM bills WHERE PaymentStatus = 'Unpaid'")
        total_unpaid_revenue = cur.fetchone()[0]
        if not total_unpaid_revenue:
            total_unpaid_revenue = 0
        if not total_paid_revenue:
            total_paid_revenue = 0
        cur.close()

        return render_template('detailed_statistics.html', total_users=total_users, unit_consumption=unit_consumption,
                               total_paid_revenue=total_paid_revenue, total_unpaid_revenue=total_unpaid_revenue)
    except Exception as e:
        error_msg = "Error occurred while fetching data: " + str(e)
        return render_template('error.html', message=error_msg)


@app.route('/admin_dashboard')
def admin_dashboard():
    # Add logic to fetch and display data for admin dashboard
    return render_template('admin_dashboard.html')

# User routes


@app.route('/client_dashboard/<int:user_id>')
def client_dashboard(user_id):
    # Fetch user information from the database using the user_id
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM users WHERE UserID = %s", (user_id,))
    user_data = cur.fetchone()
    cur.close()

    if user_data:
        # Extract user details
        username = user_data[1]
        email = user_data[3]
        address = user_data[5]
        contact_number = user_data[6]

        # Fetch bill summary for the user
        cur = mysql.connection.cursor()
        cur.execute(
            "SELECT Year, Month, BillAmount, PaymentStatus,BillID FROM bills WHERE UserID = %s", (user_id,))
        bill_summary = cur.fetchall()

        cur.close()

        return render_template('client_dashboard.html', user=username, user_id=user_id, email=email, address=address, contact_number=contact_number, bill_summary=bill_summary)
    else:
        error = 'User not found'
        return render_template('error.html', message=error)


@app.route('/make_payment/<int:user_id>/<int:bill_id>', methods=['POST'])
def make_payment(user_id, bill_id):
    if request.method == 'POST':
        # Code to update the payment status for the bill with bill_id
        cur = mysql.connection.cursor()
        try:
            cur.execute(
                "UPDATE bills SET PaymentStatus = 'Paid' WHERE BillID = %s", (bill_id,))
            mysql.connection.commit()
            cur.close()
            return redirect(url_for('client_dashboard', user_id=user_id))
        except Exception as e:
            error_msg = "Error occurred while making payment: " + str(e)
            return render_template('error.html', message=error_msg)
    else:
        # Handle GET request, if necessary
        return redirect(url_for('client_dashboard', user_id=user_id))


if __name__ == '__main__':
    app.run(debug=True)
