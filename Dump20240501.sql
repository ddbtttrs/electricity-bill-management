-- MySQL dump 10.13  Distrib 8.0.36, for macos14 (arm64)
--
-- Host: localhost    Database: electricity_bill_mgmt
-- ------------------------------------------------------
-- Server version	8.3.0

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admins`
--

DROP TABLE IF EXISTS `admins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admins` (
  `AdminID` int NOT NULL AUTO_INCREMENT,
  `Username` varchar(50) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `FullName` varchar(100) NOT NULL,
  PRIMARY KEY (`AdminID`),
  UNIQUE KEY `Username` (`Username`),
  UNIQUE KEY `Email` (`Email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admins`
--

LOCK TABLES `admins` WRITE;
/*!40000 ALTER TABLE `admins` DISABLE KEYS */;
INSERT INTO `admins` VALUES (1,'HP007','$2b$12$4IoVMF49yvrW/cegYKhDeuPgDdTPXRZVg9iAVaJK9DKNoAa0embNC','harry@potter.com','HP007');
/*!40000 ALTER TABLE `admins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bills`
--

DROP TABLE IF EXISTS `bills`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bills` (
  `BillID` int NOT NULL AUTO_INCREMENT,
  `UserID` int DEFAULT NULL,
  `Year` int DEFAULT NULL,
  `Month` int DEFAULT NULL,
  `BillAmount` decimal(10,2) DEFAULT NULL,
  `PaymentStatus` enum('Paid','Unpaid') DEFAULT NULL,
  PRIMARY KEY (`BillID`),
  KEY `UserID` (`UserID`),
  CONSTRAINT `bills_ibfk_1` FOREIGN KEY (`UserID`) REFERENCES `users` (`UserID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bills`
--

LOCK TABLES `bills` WRITE;
/*!40000 ALTER TABLE `bills` DISABLE KEYS */;
INSERT INTO `bills` VALUES (1,1,2024,4,2500.00,'Paid'),(2,3,2024,4,1125.00,'Paid'),(3,5,2024,4,12500.00,'Unpaid'),(4,2,2024,4,125.00,'Unpaid'),(5,4,2024,4,200.00,'Unpaid'),(6,11,2024,4,2500.00,'Paid');
/*!40000 ALTER TABLE `bills` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `UserID` int NOT NULL AUTO_INCREMENT,
  `Username` varchar(50) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `FullName` varchar(100) NOT NULL,
  `Address` varchar(255) DEFAULT NULL,
  `ContactNumber` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`UserID`),
  UNIQUE KEY `Username` (`Username`),
  UNIQUE KEY `Email` (`Email`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'ronweasley5','$2b$12$Bis5vqrsuplHVNlwiXG5huLgv801Ab8c5KA0hPYS/7kaeuO6eCb5y','ron@weasley.com','Ronald Bilius Weasley','5 Quidditch Pkwy','8007009000'),(2,'ssnape5','$2b$12$tWsVoC0g4OGOSP.FbnF1HO2kyQgcsMbRSevO2TZbSd56xkBNVuxay','severus@snape.com','Severus Snape','13 Hogwarts Way','7006786789'),(3,'hpotter5','$2b$12$OV2R.QhzKIzD413vZGTzfeC.YDFKKjKhdl2ivPpnWVi1zzlOYiKHy','harry@potter.com','Harry Potter','12 British Rd','1212343499'),(4,'thead33','$2b$12$yl7pXzv4hjNUfF8qkOo46.4evGZlnxY9nrk4lrLhU9qr.ORJs5upu','travis@head.com','Travis Head','800 Brisbane Dr','9007891234'),(5,'mkbhd12','$2b$12$.KMHZwzq.FbiJ8vNrKnP7uBIbSBYnHEkgAt9mpw4/DHdRhgIxZHFG','mkbhd@email.com','Marques Brownlee II','800 Bahama Pkwy','900989800'),(6,'blee5','$2b$12$5ozZtEd/suhkKAAN5ZLWCOj0VHLCju2VymW5t.KmXX.z9YId0PmNa','blee5@usa.com','Bruce Lee IV','1234 Evergrande St','1280133293'),(7,'marilynm7','$2b$12$mGKODarJ5Pyl6IJbtyeTh.9RSHDdOK6rjYIjDw3rU9IJT9DIME/pu','marilynm7@people.com','Marilyn Monroe ','99 Hollywood Blvd','8012323389'),(8,'james55','$2b$12$1GkiPCCcuwBauUR6EfoTZOaMjsseB9BInRhOvzID0BwhIEXzVHZ6W','james55@yahoo.com','James McCain','3512 Dairy Blvd','7982127321'),(9,'slayyqueen19','$2b$12$HSGN4vi9rsdhjEJA3VdgXeorJd9znI9/kNHpSQuZyXwFHRGMas06G','mam@mam.com','Bonita Roberts','22 Glass Blvd','7127891212'),(10,'rypatriot29','$2b$12$6SAOxEhE6cVZgBYmf4VQLecrcCxXjDTda16ugF8/bsTBmpQai7o3W','rypatriot29@gmail.com','Ryan Patriot Sr.','19 Microsoft Dr','8009812548'),(11,'bingqilin29','$2b$12$XuD91WUWsEhGh2zCS8Rub.XkJTmBo8lmfMeGz920SiwAo90YH4UYa','bingqilin29@iccream.ch','BingQilin Vanilla','44 South Texas Pkwy','9032349012'),(14,'jonbones7','$2b$12$ICCwJ8tTpGCE3Ecb3RDn4.nBwQIBqe7.rPk1UUdeQNv3Lrs9o/QDi','jonbones7@gmail.com','Jonny Bones','1218 Arkansas Dr','9009876262'),(15,'ccc999','$2b$12$DSr3w65Cy1Mq6MMwAueyKuRKs2k6DhLM7EX2uffj6VVQO8OtkG7M6','ccc999@gmail.com','Henry Cejudo IV','1919 Palm Dr','8007643222'),(16,'coli57','$2b$12$VNpP4lkVrEo90rLpGWenKevXq11nAQIYdy8uEQYbNGbQNv0ua3knS','coli57@usa.com','Charlie Oliveira','2922 WestCreek Dr','7006589011'),(17,'jpenny55','$2b$12$hsg87Tdl.wX0COjWpDd/hetLk9fgiXDK13TtKnl6G8RoPlVZBlLoC','jpenny55@hotmail.com','Joseph Penny','1900 University Blvd','9003458800'),(18,'ddeg89','$2b$12$n4.AA0c0vNwMXkk5XhcZs.KARL4LON6Y4KSXh2HP/WlJhV1OcN2mq','ddeg89@gmail.com','David DeGracie','3255 Coconut Creek','6597089090');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-05-01 22:32:06
